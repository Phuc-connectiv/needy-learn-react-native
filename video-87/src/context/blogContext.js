import React, { useState, useReducer } from 'react';
import CreateDataContext from './createDataContext';
import jsonServer from '../api/jsonServer';

const BlogReducer = (state, action) => {
    switch (action.type) {
        case 'get': return action.payload;
        case 'add': return [
            ...state,{
            id: Math.floor(Math.random() * 999),
            title: action.payload.title, 
            content: action.payload.content
        }];
        case 'delete': return state.filter(blogPost => blogPost.id !== action.payload );
        case 'edit': return state.map((blogPost) => {
            return blogPost.id === action.payload.id ? action.payload : blogPost;
        });
        default: return state;
    }
};
const getBlogPosts = dispatch => {
    return async () => {
        const res = await jsonServer.get('/blogPost');
        dispatch({type:'get', payload: res.data });
    };
};
const addBlogPost = () => {
    return async (title, content, callback) => { 
        await jsonServer.post('/blogPost', {title, content})
        ispatch({ type: 'add', payload: {title, content}});
           if(callback){
            callback();
           }     
        } 
};
const deleteBlogPost = (dispatch) => {
    return async (id) => {
        await jsonServer.delete(`/blogPost/${id}`)
        dispatch({ type: 'delete', payload: id });
    }
};
const editBlogPost = (dispatch) => {
    return async (id, title, content, callback) => {
        await jsonServer.put(`/blogPost/${id}`, {title, content});
        dispatch({ type: 'edit' , payload: { id: id, title: title, content: content } });
        callback();
    }
};
export const { Context, Provider } = CreateDataContext(
    BlogReducer,
    {  addBlogPost, deleteBlogPost, editBlogPost, getBlogPosts },
    []
);

//--------------------------------------------------
// const BlogReducer = (state, action) => {
//     switch (action.type) {
//         case 'add': return[...state,{ id: Math.floor(Math.random() * 99999 ),title: `# ${state.length + 1}`}];
//         case 'delete': return state.filter((blogPost) => {
//             return blogPost.id !== action.payload
//         });
//         default: return state;
//     }
// };
// const BlogConText = React.createContext();
// export const BlogProvider = ({ children }) => {
//     const [ blogPosts, dispatch ] = useReducer(BlogReducer, []);
//     const addBlogPost = () => {
//         dispatch({type: 'add'});
//     }
//     const deleteBlogPost = () => {
//         return (id) => {
//         dispatch({type: 'delete', payload: id});
//         }
//     }
//     return(
//         <BlogConText.Provider value={{ data: blogPosts, addBlogPost: addBlogPost, deleteBlogPost:deleteBlogPost}}>
//             {children}
//         </BlogConText.Provider>
//     );
// }
// export default BlogConText;
// ------------------------------------------------------
//     const BlogConText = React.createContext();
//     export const BlogProvider = ({ children }) => {
//     const [ blogPosts, setBlogPosts ] = useState([]);
//     const addBlogPost = () => {
//         setBlogPosts([...blogPosts, {id: Math.floor(Math.random() * 99999 ),title: `blog post ${blogPosts.length + 1}` }]);
//     }
//     return(
//         <BlogConText.Provider value={{ data: blogPosts, addBlogPost: addBlogPost}}>
//             {children}
//         </BlogConText.Provider>
//     );
// }
// export default BlogConText;