import React, { Component } from 'react';
import {
    Text
} from 'react-native';
import firebase from 'firebase';
import {
    Button, Card, CardSection, Input, Spinner
} from './common'

class LoginForm extends Component {
    state = { mail: '', password: '', error: '', loading: false };
    onButtonPress() {
        const { mail, password } = this.state;
        this.setState({ error: '', loading: true });
        firebase.auth().signInWithEmailAndPassword(mail, password)
            .then(this.onLoginSuccess.bind(this))
            .catch(()=> {
                firebase.auth().createUserWithEmailAndPassword(mail, password)
                    .then(this.onLoginSuccess.bind(this))
                    .catch(this.onLoginFail.bind(this));
            });
    };
    onLoginSuccess() {
        this.setState({
            mail: '',
            password: '',
            loading: false,
            error: ''
        });
    };
    onLoginFail() {
        this.setState({ error: 'Authentication Failed', loading: false });
    };
    renderButton() {
        if(this.state.loading){
            return <Spinner size="small"/>
        }
        return (
            <Button 
                name={'Login'}
                onPress={this.onButtonPress.bind(this)}
            />
        );
    };
    render() {
        return (
            <Card>
                <CardSection>
                    <Input
                        placeholder="user@gmail.com"
                        label={'Email'}
                        value={this.state.mail}
                        style={{ height: 20, width: 100 }}
                        onChangeText={mail => this.setState({ mail })}
                    />
                </CardSection>
                <CardSection>
                    <Input
                        secureTextEntry
                        placeholder="*********"
                        label={'Password'}
                        value={this.state.password}
                        style={{ height: 20, width: 100 }}
                        onChangeText={password => this.setState({ password })}
                    />
                </CardSection>
                <Text style={styles.errorStyle}>
                    {this.state.error}
                </Text>
                <CardSection>
                   {this.renderButton()}
                </CardSection>
            </Card>
        );
    }
}
const styles = {
    errorStyle: {
        fontSize: 20,
        alignItems: 'center',
        color: 'red'
    }
}
export default LoginForm;