import React, { useContext, useState } from 'react';
import { StyleSheet, Text, TextInput, View, Button, Alert } from 'react-native';
import { Context } from '../context/blogContext';
 
const BlogPostFrom = ({ onSubmit, initValue, lable, text }) => {
    const [ title, setTitle ] = useState(initValue.title);
    const [ content, setContent ] = useState(initValue.content);
    return (
        <View>
            <Text style={{fontSize: 30}}>{lable} Screen</Text>
            <Text>Enter Title:</Text>
            <TextInput 
                style={styles.input} 
                value={title} 
                onChangeText={(text) => setTitle(text)}
            />
            <Text>Enter Content:</Text>
            <TextInput 
                style={styles.input}
                value={content} 
                onChangeText={(text) => setContent(text)}
            />
            <Button 
                title={text}
                onPress={() => onSubmit(title, content)}
            />
        </View>
    );
}
BlogPostFrom.defaultProps = {
    initValue: {
        title: '',
        content: '',
    },
};
export default BlogPostFrom;
const styles = StyleSheet.create({
   input:{
       borderWidth:1,
       marginHorizontal: 5,
       marginVertical: 5,
       padding: 5
   }
  });