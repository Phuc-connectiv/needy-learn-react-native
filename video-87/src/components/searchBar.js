import React from 'react';
import { StyleSheet, Text, View, TextInput } from 'react-native';
import { Feather } from '@expo/vector-icons';

const SearchBar = ({term, onTermChange, onTermSubmit}) => {
  return (
    <View style={styles.container}>
      <Feather 
        name="search" 
        size={30}
        style={styles.icon}
      />
      <TextInput 
        value={term}
        autoCapitalize="none"
        autoCorrect={false}
        placeholder="search"
        style={styles.input}
        onChangeText={onTermChange}
        onEndEditing={onTermSubmit}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#DDD',
    height: 50,
    borderRadius: 5,
    marginHorizontal: 15,
    flexDirection: 'row',
    alignItems: 'center'
  },
  input: {
      flex: 1,
  },
  icon: {
      margin: 10
  }
});
export default SearchBar;