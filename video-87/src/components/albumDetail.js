import React from 'react';
import {
    View, Text, Image, Linking
} from 'react-native';
import Card from '../components/card';
import CardSection from './cardSection';
import Button from './button';

const AlbumDetail = ({album}) => {
    const { title, artist, thumbnail_image, image, url } = album;
    const { 
        thumbnaiStyle,
        albumContainerStyle,
        headerTextStyle,
        imageStyle
    } = styles;
    return (
        <Card>
            <View>
                <CardSection>
                    <View style={albumContainerStyle}>
                        <Image 
                            source={{ uri: thumbnail_image }}
                            style={ thumbnaiStyle }
                        />
                    </View>
                    <View style={styles.viewText}>
                        <Text style={headerTextStyle}>{ title }</Text>
                        <Text>{ artist }</Text>
                    </View>
                </CardSection>
                <CardSection>
                    <Image 
                        source={{ uri: image }}
                        style={ imageStyle }
                    />
                </CardSection>
                <CardSection>
                    <Button 
                        onPress={() => Linking.openURL(url)}
                        name={'Buy Now'} 
                    />
                </CardSection>
            </View>
        </Card>
    );
}
const styles={
    viewText:{
        flexDirection: 'column',
        justifyContent: 'space-around'
    },
    thumbnaiStyle: {
        width: 50,
        height: 50
    },
    headerTextStyle: {
        fontSize: 20
    },
    albumContainerStyle: {
        justifyContent: 'center',
        alignItems: 'center',
        marginLeft: 10,
        marginRight: 10
    },
    imageStyle: {
        height: 300,
        flex: 1,
        width: null
    }
}
export default AlbumDetail;