import React, { useContext, useEffect } from 'react';
import { 
    StyleSheet,
     Text, 
     View, 
     FlatList, 
     Button, 
     TouchableOpacity,
     StatusBar
 } from 'react-native';
import { Context } from '../context/blogContext';
import { Feather } from '@expo/vector-icons';
import HeaderScreen from './header';
import AlbumList from './albumsList';

const IndexScreen = ({navigation}) => {

    const { state, deleteBlogPost, getBlogPosts } = useContext(Context);
    useEffect(() => {
        getBlogPosts();
        const listener = navigation.addListener('didFocus', () => {
            getBlogPosts();
        })
        return () => {
            listener.remove();
        };
    }, []);
    return (
        <View style={{ flex: 1 }} >
            <StatusBar barStyle="dark-content" />
            <Button 
                title="Login"
                onPress={() => navigation.navigate('Login')}
            />
            <Button 
                title="Add post"
                onPress={() => navigation.navigate('Create')}
            />
            <FlatList 
                data={state}
                keyExtractor={(blogPost) => blogPost.title}
                renderItem = {({item}) => {
                return (
                    <TouchableOpacity
                        onPress={() => navigation.navigate('Show',{id: item.id})}
                    >
                        <View style={styles.content}>
                            <Text>{item.title} - {item.id}</Text>
                            <TouchableOpacity onPress={() => deleteBlogPost(item.id)}>
                                <Feather name="trash" size={25} />
                            </TouchableOpacity>
                        </View>
                    </TouchableOpacity>
                )
                }}
            />
            <HeaderScreen textHeader={'Albums'}/>
            <AlbumList />
        </View>
    );
}
IndexScreen.navigationOptions = ({ navigation }) => {
    return {
        headerRight: (
            <View>
                <TouchableOpacity onPress={() => navigation.navigate('Create')}>
                <Feather name="plus" size={30}/>
                </TouchableOpacity>
            </View>
        )
    };
};
export default IndexScreen;
const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#fff',
      alignItems: 'center',
      justifyContent: 'center',
    },
    content: {
        paddingTop: 10,
        paddingBottom:10,
        alignItems: 'center',
        justifyContent: 'space-around',
        flexDirection: 'row',
        borderBottomWidth: 1,
        borderTopWidth: 1
    }
});