import React, { useState, useEffect } from 'react';
import { StyleSheet, Text, View } from 'react-native';
import SearchBar from '../components/searchBar';

const SearchScreen = () => {
  const [ term, setTerm ] = useState('');
  const [ errMessage, setErrMessage ] = useState('');
  const [ results, setResults ] = useState('');
  const searchApi = async (searchTerm) => {
    try {
    const response = await yelp.get('/Search', {
      params: {
        limit: 50,
        term: searchTerm,
        location: 'san jose'
      }
    });
    setResults(response.data.businesses);
  } catch (err) {
      setErrMessage('sorry faild');
  }
}
useEffect(() => {
  searchApi('pasta');
}, []);
  return (
    <View >
        <SearchBar 
            term={term}
            onTermChange={setTerm}
            onTermSubmit={(term) => searchApi(term)}
        />
        <Text>Search Screen: {term}</Text>
        <Text>We have found {results.length} results</Text>
        {errMessage ? <Text>{errMessage}</Text> : null}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
export default SearchScreen;