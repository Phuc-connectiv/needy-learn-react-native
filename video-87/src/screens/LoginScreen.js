import React from 'react';
import {
    View, Text
} from 'react-native';
import firebase from 'firebase';
import { Header, Button, Spinner } from '../components/common';
import LoginForm from '../components/loginForm';

class LoginScreen extends React.Component {
    state = { loggedIn: false }
    componentWillMount() {
        firebase.initializeApp({
            apiKey: 'AIzaSyC5Atkw-f5lxL9rhCyCrywwbQSnCjAd7dc',
            authDomain: 'authentication-df2dd.firebaseapp.com',
            databaseURL: 'https://authentication-df2dd.firebaseio.com',
            projectId: 'authentication-df2dd',
            storageBucket: 'authentication-df2dd.appspot.com',
            messagingSenderId: '948767360900',
            appId: '1:948767360900:web:255c5aaf448a5451e63fd1',
            measurementId: 'G-34N0CFNT10'
          });
          firebase.auth().onAuthStateChanged((user)=> {
            if(user) {
                this.setState({ loggedIn: true });
            }else {
                this.setState({ loggedIn: false });
            }
          });
    }
    renderContent() {
       switch(this.state.loggedIn) {
           case true: return (
            <Button 
                name="Log Out"
                onPress={()=> firebase.auth().signOut()}
            />
           );
           case false: return <LoginForm />
           default: return <Spinner size="large"/>
       }
    };
    render() {
        return (
            <View>
                <Header textHeader={'Authentication'}/>
                { this.renderContent() }
            </View>
        );
    };
};
export default LoginScreen;