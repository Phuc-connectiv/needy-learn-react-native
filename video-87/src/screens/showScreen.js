import React, { useContext } from 'react';
import { StyleSheet, Text, View, TouchableOpacity } from 'react-native';
import { Context } from '../context/blogContext';
import { EvilIcons } from '@expo/vector-icons';
 
const ShowScreen = ({ navigation }) => {
    const { state } = useContext(Context);
    const blogPost = state.find((blogPost) => {
        return blogPost.id === navigation.getParam('id')
    });
    return (
        <View style={styles.container}>
            <Text style={{fontSize: 30}}>Show Screen</Text>
            <Text style={{fontSize: 15}}>{blogPost.title}</Text>
            <Text style={{fontSize: 15, color: 'red'}}>{blogPost.content}</Text>
        </View>
    );
}
ShowScreen.navigationOptions = ({ navigation }) => {
    return {
        headerRight: (
            <TouchableOpacity onPress={() => navigation.navigate('Edit', {id: navigation.getParam('id')})}>
                <EvilIcons style={{marginRight: 5}} name="pencil" size={30} />
            </TouchableOpacity>
        )
    }
}
export default ShowScreen;
const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#fff',
      alignItems: 'center',
      justifyContent: 'center',
    },
    content: {
        paddingTop: 10,
        paddingBottom:10,
        alignItems: 'center',
        justifyContent: 'space-around',
        flexDirection: 'row',
        borderBottomWidth: 1,
        borderTopWidth: 1
    }
  });