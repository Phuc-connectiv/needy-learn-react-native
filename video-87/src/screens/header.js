import React from 'react';
import { View, Text } from 'react-native';

const HeaderScreen = (props) => {
    const { test, font } = styles;
    return (
        <View style={test}>
            <Text style={font}>{props.textHeader}</Text>
        </View>
    ); 
}
const styles = {
    test: {
        backgroundColor: '#F8F8F8',
        justifyContent: 'center',
        alignItems: 'center',
        height: 60,
        paddingTop: 5,
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.2,
        elevation: 2,
        position: 'relative'
    },
    font: {
        fontSize: 24,
    }
}
export default HeaderScreen;