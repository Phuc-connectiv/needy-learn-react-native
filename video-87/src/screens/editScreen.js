import React, { useContext, useState } from 'react';
import { StyleSheet, Text, TextInput, View, Button, Alert } from 'react-native';
import { Context } from '../context/blogContext';
import BlogPostFrom from '../components/blogPostForm';
 
const EditScreen = ({ navigation }) => {
    const { state, editBlogPost }  = useContext(Context);
    const id = navigation.getParam('id');
    const blogPost = state.find(
        blogPost => blogPost.id === id
    );
    const [ title, setTitle ] = useState(blogPost.title);
    const [ content, setContent ] = useState(blogPost.content);
    return (
       <BlogPostFrom 
        initValue={{ title: blogPost.title, content: blogPost.content }}
        onSubmit={(title, content ) => {
            editBlogPost(id, title, content, () => {
                navigation.navigate('Show')
            });
        }}
        lable='Edit'
        text='Save Post'
        />
    );
}
export default EditScreen;
const styles = StyleSheet.create({
   input:{
       borderWidth:1,
       marginHorizontal: 5,
       marginVertical: 5,
       padding: 5
   }
  });