import React, { useContext, useState } from 'react';
import { StyleSheet, Text, TextInput, View, Button, Alert } from 'react-native';
import { Context } from '../context/blogContext';
import BlogPostFrom from '../components/blogPostForm';
 
const CreateScreen = ({ navigation }) => {
    const { addBlogPost } = useContext(Context);
   
    return (
       <BlogPostFrom 
        onSubmit={(title, content) => {
            addBlogPost(title, content, ()=> {
                navigation.navigate('Index')
            });
        }}
        lable='Create'
        text='Add Post'
       />
    );
}
export default CreateScreen;
const styles = StyleSheet.create({
   input:{
       borderWidth:1,
       marginHorizontal: 5,
       marginVertical: 5,
       padding: 5
   }
  });