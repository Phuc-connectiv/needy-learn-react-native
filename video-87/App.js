import React from 'react';
import { StyleSheet, Text, View, TouchableOpacity } from 'react-native';
import { createStackNavigator } from 'react-navigation-stack';
import SearchScreen from './src/screens/searchScreen';
import { createAppContainer } from 'react-navigation';
import IndexScreen from './src/screens/indexScreen';
import { Provider } from './src/context/blogContext';
import ShowScreen from './src/screens/showScreen';
import CreateScreen from './src/screens/createScreen';
import Feather from '@expo/vector-icons';
import EditScreen from './src/screens/editScreen';
import HeaderScreen from './src/screens/header';
import AlbumList from './src/screens/albumsList';
import LoginScreen from './src/screens/LoginScreen';

const navigator = createStackNavigator({
  Search: {
    screen: SearchScreen,
  },
  Index: IndexScreen,
  Show: ShowScreen,
  Create: CreateScreen,
  Edit: EditScreen,
  Header: HeaderScreen,
  Album: AlbumList,
  Login: LoginScreen,
},
{
  initialRouteName: 'Index',
  defaultNavigationOptions: {
    title: 'Business Blogs',
    headerStyle: {
      backgroundColor: 'red'
    }
  }
}
);
const App = createAppContainer(navigator);
export default () => {
  return <Provider>
      <App />
  </Provider>
}