import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import reducers from './src/reducers';
import { Provider } from 'react-redux';
import { createStore } from 'redux';
import {
  Header
} from './src/components/common';
import LibrariesList from './src/components/librariesList';

export default function App() {
  return (
   <Provider store={ createStore(reducers) }>
     <View style={{flex:1}}>
        <Header textHeader="Tect stack"/>
        <LibrariesList/>
     </View>
   </Provider>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
