import { combineReducers } from 'redux';
import Libraries from './libraries';
import SelectionReducer from './selectionReducer';

export default combineReducers({
    libraries: Libraries,
    SelectionLibraryId: SelectionReducer
})