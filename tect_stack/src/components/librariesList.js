import React from 'react';
import { connect } from 'react-redux';
import {
    FlatList, Text
} from 'react-native';
import ListItem from './listItem';
import {
    Card
} from '../components/common';

class LibrariesList extends React.Component {
    renderItem(item) {
        return <Card><ListItem item={item}/></Card>;
    }
    render() {
        return(
            <FlatList 
                data={this.props.libraries}
                renderItem={({item}) => this.renderItem(item)}
                keyExtractor={(item) => item.id}
            />
        );
    };
};
const mapStateToProps = state => {
    return { libraries: state.libraries };
};
export default connect(mapStateToProps)(LibrariesList);