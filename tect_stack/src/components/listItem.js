import React from 'react';
import {
    View, Text, TouchableWithoutFeedback
} from 'react-native';
import {
    CardSection, Card
} from './common';
import  * as action from '../action';
import { connect } from 'react-redux';

const ListItem = (props) => {
    const renderDescription =() => {
        const { item, selectLibraryId } = props
       if(item.id === selectLibraryId){
           return (
           <Text>{ item.description }</Text>
           );
       } 
    };
    const { titleStyle } = styles; 
    const { id, title } = props.item; 
    return (
        <TouchableWithoutFeedback
            onPress={() => this.props.selectLibrary(id)}
        >
            <CardSection>
                <Text style={titleStyle}>
                    {title }
                </Text>
            </CardSection>
        </TouchableWithoutFeedback>
    );
};
const styles = {
    titleStyle: {
        fontSize: 18,
        paddingLeft: 15 
    }
};
const mapStateToProps = state => {
    return { selectLibraryId: state.selectLibraryId }
};
export  default connect(mapStateToProps, action)(ListItem);