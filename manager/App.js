import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import reducers from './src/reducers';
import firebase from 'firebase';
import LoginForm from './src/components/loginForm';
import { 
  Header
} from './src/components/common';
import ReduxThunk from 'redux-thunk';

class App extends React.Component {
  componentDidMount() {
    const config = {
      apiKey: "AIzaSyDxpZEjnv-1QAkN8yspL_rk1en2Nv0OAzQ",
      authDomain: "manager-7cc3f.firebaseapp.com",
      databaseURL: "https://manager-7cc3f.firebaseio.com",
      projectId: "manager-7cc3f",
      storageBucket: "manager-7cc3f.appspot.com",
      messagingSenderId: "111742288550",
      appId: "1:111742288550:web:357266b95f05413cb9ea67",
      measurementId: "G-DNWJPY7TFB"
    };
    firebase.initializeApp(config);
  };
    render() {
      const store= createStore(reducers, {}, applyMiddleware(ReduxThunk));
      return (
       
        <Provider store={store}>
            <Header textHeader="LOGIN"/>
            <LoginForm />
        </Provider>
      );
    };
}
export default App;
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
