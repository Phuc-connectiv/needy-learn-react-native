import React from 'react';
import {
  View, Text, StyleSheet, Button,
  TouchableOpacity,
  Dimensions
} from 'react-native';
import { createStackNavigator } from 'react-navigation-stack';
import IndexScreen from './src/screen/indexScreen';
import { createAppContainer, createSwitchNavigator } from 'react-navigation';
import SplashScreen from './src/screen/splashScreen';
import { Provider } from './src/context/blogContext';
import ShowScreen from './src/screen/showScreen';
import CreateScreen from './src/screen/createScreen';
import { Feather, EvilIcons } from '@expo/vector-icons';
import EditScreen from './src/screen/edit';
import { createBottomTabNavigator } from 'react-navigation-tabs';
import AlbumScreen from './src/screen/albumScreen';
import { createDrawerNavigator } from 'react-navigation-drawer';
import Bars from './src/component/bars';
import ImageScreen from './src/screen/imageScreen';
import IntroScreen from './src/screen/introScreen';
import LoginScreen from './src/screen/loginScreen';
import LogoutScreen from './src/screen/logOutScreen';
import LibraryScreen from './src/screen/libraryScreen';
import appSignin from './src/screen/appSignin';
import GetImageScreen from './src/screen/getImageScreen';

const SignInStack = createStackNavigator({
  SignIn:  {
    screen: appSignin
  }
},{
  initialRouteName: 'SignIn',
  defaultNavigationOptions: {
    title: 'SignIn'
}});

const homStack = createStackNavigator({
  Index: {
    screen: IndexScreen,
    navigationOptions: ({ navigation }) => {
      return {
        title: 'Home',
        headerRight: <TouchableOpacity onPress={() => navigation.navigate('Create')}>
          <Feather name="plus" size={30} />
        </TouchableOpacity>
      };
    }
  },
  Splash: SplashScreen,
  Show: {
    screen: ShowScreen,
    navigationOptions: ({ navigation }) => {
      return {
        title: 'Show',
        headerRight: <TouchableOpacity onPress={() => navigation.navigate('Edit', { id: navigation.getParam('id') })}>
          <EvilIcons name="pencil" size={30} />
        </TouchableOpacity>
      }
    }
  },
  Create: {
    screen: CreateScreen,
    navigationOptions: {
      title: 'Create'
    }
  },
  Edit: EditScreen
}, {
  initialRouteName: 'Index',
  defaultNavigationOptions: {
    title: 'Home',
    headerStyle: {
      backgroundColor: '#ddd'
    }
  }
});
const albumStack = createStackNavigator({
  Album: {
    screen: AlbumScreen,
    navigationOptions: {
      headerShown: false
    }
  }
});
const tabNavigator = createBottomTabNavigator({
  Home: {
    screen: homStack,
    navigationOptions: {
      tabBarIcon: ({ tintColor }) => <Feather name="user" size={16} color={tintColor} />
    }
  },
  AlBum: {
    screen: albumStack,
    navigationOptions: {
      tabBarIcon: ({ tintColor }) => <Feather name="list" size={16} color={tintColor} />
    }
  },
  SignIn: {
    screen: SignInStack,
    navigationOptions: {
      tabBarIcon: ({ tintColor }) => <Feather name="list" size={16} color={tintColor} />
    }
  }
}, {
  tabBarOptions: {
    showLabel: false, // hide labels
    activeTintColor: 'red', // active icon color
    inactiveTintColor: '#fff',  // inactive icon color
    style: {
      backgroundColor: '#ddd' // TabBar background
    }
  }
});
const drawer = createDrawerNavigator({
  One: {
    screen: tabNavigator,
    navigationOptions: {
      drawerIcon: ({ tintColor }) => <Feather name="user" size={24} color={tintColor} />
    }
  },
  two: {
    screen: ImageScreen,
    navigationOptions: {
      drawerIcon: ({ tintColor }) => <Feather name="message-square" size={24} color={tintColor} />,
    }
  },
  Logout: {
    screen: LogoutScreen,
    navigationOptions: {
      drawerIcon: ({ tintColor }) => <Feather name="list" size={24} color={tintColor} />,
    }
  },
  Image: {
    screen: GetImageScreen,
    navigationOptions: {
      drawerIcon: ({ tintColor }) => <Feather name="list" size={24} color={tintColor} />,
    }
  }
}, {
  contentComponent: props => <Bars {...props} />,
  drawerWidth: Dimensions.get('window').width * 0.85,
  drawerBackgroundColor: '#ddd',
  hideStatusBar: true,
  contentOptions: {
    activeTintColor: "red",
    activeBackgroundColor: 'rgba(212,118,207, 0.2)',
    itemsContainerStyle: {
      marginTop: 10,
      marginHorizontal: 10
    },
    itemStyle: {
      borderRadius: 5
    }
  }
});
const IntroStack = createStackNavigator({
  Intro: {
    screen: IntroScreen,
  },
  After: drawer,
  Login: LoginScreen,
},{
  initialRouteName: 'Intro',
  defaultNavigationOptions: {
    headerShown: false
  }
});
const AppContainer = createAppContainer(IntroStack);
const styles = StyleSheet.create({
  appStyle: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  }
});
const AppComponent = () => {
  return (
    <Provider>
      <AppContainer />
    </Provider>
  );
};
export default AppComponent;
