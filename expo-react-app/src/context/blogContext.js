import createDataContext from './createDataContext';
import jsonServer from '../api/jsonServer';

const blogReducer = ( state, action ) => {
    switch(action.type) {
        case 'get':
            return action.payload;
        case 'add':
            return [ 
                ...state, { 
                    title: action.payload.title,
                    context: action.payload.context,
                    id: Math.floor(Math.random() * 99 )
                }
            ];
        case 'delete':
            return state.filter((blogPost) => blogPost.id !== action.payload);
        case 'edit':
            return state.map((blogPost) => {
               return blogPost.id === action.payload.id ? action.payload : blogPost;
            });
        default:
            return state;
    }
};
const getBlogPost = dispatch => {
    return async() => {
        const res = await jsonServer.get('/blogposts');
        dispatch({ type: 'get', payload: res.data });
    }
};
const addBlogPost = dispatch => {
    return async (title, context, callback) => {
        await jsonServer.post('/blogposts', { title, context })
        // dispatch({ type: 'add', payload: { title, context }});
        if(callback){
            callback();
        }
    };
 };
const deleteBlogPost = dispatch => {
    return async id => {
        await jsonServer.delete(`/blogposts/${id}`);
    //    dispatch({ type: 'delete', payload: id});
    };
};
const editBlogPost = dispatch => {
    return async (id, title, context, callback) => {
        await jsonServer.put(`/blogposts/${id}`, {  title, context });
        // dispatch({ type: 'edit', payload: { id, title, context }});
        if(callback){
            callback();
        }
    }
};
export const { Context, Provider } = createDataContext(
    blogReducer,
    {
        addBlogPost,
        deleteBlogPost,
        editBlogPost,
        getBlogPost
    },
    []
);



//__________________________________AUTOMATING CONTEXT CREATION___________________________________
// import React, { useReducer } from 'react';

// const BlogContext = React.createContext();
// const blogReducer = ( state, action ) => {
//     switch(action.type) {
//         case 'add':
//             return [ ...state, { title: `Blog Post #${ state.length + 1 }` }];
//         default: 
//             return state;
//     }
// };
// export const BlogProvider = ({ children }) => {
//     const [ blogPosts, dispatch ] = useReducer( blogReducer, []);
//     const addBlogPost = () => {
//        dispatch({ type: 'add' });
//     };
//     return (
//         <BlogContext.Provider value={{ data: blogPosts, addBlogPost }}>
//             { children }
//         </BlogContext.Provider>
//     );
// };
// export default BlogContext;



//__________________________________________USE STATE__________________________________________
// import React, { useState } from 'react';

// const BlogContext = React.createContext();

// export const BlogProvider = ({ children }) => {
//     const [ blogPosts, setBlogPost ] = useState([]);
//     const addBlogPost = () => {
//         setBlogPost([ ...blogPosts, { title: `Blog Post ${blogPosts.length + 1}` }]);
//     };
//     return (
//         <BlogContext.Provider value={{ data: blogPosts, addBlogPost: addBlogPost }}>
//             { children }
//         </BlogContext.Provider>
//     );
// };
// export default BlogContext;
