import React from 'react';
import {
    View, Text, StyleSheet
} from 'react-native';
import {
    Button, Card,
    CardSection,
    Input,
    Spinner
} from './common';
import firebase from 'firebase';

class LoginForm extends React.Component {
    constructor(props) {
        super(props)
    }
    state = {
        email: '',
        password: '',
        error: '',
        loading: false,
        loggedIn: false
    };
    onButtonPress() {
        const { email, password } = this.state;
        this.setState({ error: '', loading: true });
        firebase.auth().signInWithEmailAndPassword(email, password)
        .then( this.onLoginSuccess.bind(this) )
            .catch(() => {
                firebase.auth().createUserWithEmailAndPassword(email, password)
                .then( this.onLoginSuccess.bind(this) )
                    .catch( this.onLoginFailed.bind(this) )
            })
    };
    onLoginSuccess() {
        this.setState({
            email: '',
            password: '',
            loading: false,
            error: 'Login success'
        });
    };
    onLoginFailed() {
        this.setState({
            error: 'Authentication Failed',
            loading: false
        });
    };
    renderButton() {
        if (this.state.loading) {
            return <Spinner size="small" />
        }
        return (
            <Button
                name="Login"
                onPress={this.onButtonPress.bind(this)}
            />
        );
    };
    render() {
        return (
            <View style={styles.container}>
                <Card>
                    <CardSection>
                        <Input
                            style={styles.textInputStyle}
                            placeholder="user@gmail.com"
                            label="Email"
                            value={this.state.email}
                            onChangeText={email => this.setState({ email })}
                        />
                    </CardSection>
                    <CardSection>
                        <Input
                            secureTextEntry
                            placeholder="password"
                            label="Password"
                            value={this.state.password}
                            onChangeText={password => this.setState({ password })}
                        />
                    </CardSection>
                    <Text style={styles.errorStyle}>{this.state.error}</Text>
                    <CardSection>
                        {this.renderButton()}
                    </CardSection>
                </Card>
            </View>
        );
    };
};
const styles = StyleSheet.create({
    container: {
        marginVertical: 100
    },
    textInputStyle: {
        width: 100,
        height: 40
    },
    errorStyle: {
        fontSize: 20,
        color: 'red',
        alignSelf: 'center'
    }
});
export default LoginForm;
