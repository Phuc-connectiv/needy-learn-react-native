import React, { useState } from 'react';
import {
    View, Text, StyleSheet,
    TextInput, Button
} from 'react-native';

const BlogPostForm = ({ onSubmit, initialValue }) => {
    const [ title, setTitle ] = useState(initialValue.title);
    const [ context, setContext ] = useState(initialValue.context);
    return (
        <View style={styles.appStyle}>
            <Text>Enter title:</Text>
            <TextInput 
                value={ title }
                onChangeText={ (newText) => setTitle(newText)}
            />
            <Text>Enter context:</Text>
            <TextInput 
                value={ context }
                onChangeText={ (newText) => setContext(newText)}
            />
            <Button 
                title="Add Blog Post"
                onPress={ () => onSubmit( title, context )}
            />
        </View>
    );
};
BlogPostForm.defaultProps = {
    initialValue: {
        title: '',
        context: ''
    }
}
const styles = StyleSheet.create({
    appStyle: {
        flex: 1,
        marginTop: 100
    }
  });
export default BlogPostForm;