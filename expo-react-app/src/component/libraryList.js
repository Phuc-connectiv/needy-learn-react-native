import React from 'react';
import { connect } from 'react-redux';
import {
    FlatList, Text, View
} from 'react-native';
import ListItem from './listItem';

class LibraryList extends React.Component {

    render() {
        return (
            <FlatList
                data={this.props.libraries}
                renderItem={({ item }) => <ListItem library={item} />}
                keyEXtractor={(item) => item.id}
            />
        );
    };
};
const mapStateToProps = state => {
    return { libraries: state.libraries }
};
export default connect(mapStateToProps)(LibraryList);