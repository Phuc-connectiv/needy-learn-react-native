
import React from 'react';
import {
    View, Text, ScrollView
} from 'react-native';
import axios from 'axios';
import AlbumDetail from '../component/albumDetail';

class AlbumList extends React.Component {
    state = { albums: [] };
    componentDidMount () {
        axios.get('http://rallycoding.herokuapp.com/api/music_albums')
        .then(res => this.setState({ albums: res.data }));
    };
    renderAlbum() {
        return this.state.albums.map(album => <AlbumDetail key={album.title} album={ album } />);
    };
    render() {
        return (
            <ScrollView>
            <View>
               { this.renderAlbum() } 
               { this.renderAlbum() } 
               { this.renderAlbum() } 
            </View>
            </ScrollView>
        );
    };
};
export default AlbumList;