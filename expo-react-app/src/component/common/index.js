import React from 'react';
import Header from './header';

export * from './button';
export * from './card';
export * from './cardSection';
export * from './header';
export * from './input';
export * from './spinner';

// export const ProfileScreen = ({ navigation }) => <Header navigation = {navigation} name="Profile"/>;
// export const MessageScreen = ({ navigation }) => <Header navigation = {navigation} name="Message"/>;
// export const ActivityScreen = ({ navigation }) => <Header navigation = {navigation} name="Activity"/>;
// export const ListScreen = ({ navigation }) => <Header navigation = {navigation} name="List"/>;
// export const ReportScreen = ({ navigation }) => <Header navigation = {navigation} name="Report"/>;
// export const SignOutScreen = ({ navigation }) => <Header navigation = {navigation} name="SingOut"/>;