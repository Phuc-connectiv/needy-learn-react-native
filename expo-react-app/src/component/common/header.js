import React from 'react';
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';
import {
    Feather, FontAwesome5
} from '@expo/vector-icons';

const Header = (props) => {
    ;
    return (
        <View style={styles.test}>
            <View>
                <Text style={styles.font}>{props.textHeader}</Text>
            </View>
            <TouchableOpacity
               onPress={props.navigation.openDrawer}
            >
                <View style={{ flexDirection: "row", justifyContent: 'flex-end' }}>
                    <FontAwesome5 name="bars" size={25} />
                </View>
            </TouchableOpacity>
        </View>
    );
}
const styles = StyleSheet.create({
    test: {
        backgroundColor: '#F8F8F8',
        justifyContent: 'space-between',
        alignItems: 'center',
        flexDirection: 'row',
        height: 100,
        paddingTop: 5,
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.2,
        elevation: 2,
        position: 'relative',
        paddingLeft: 10,
        paddingRight: 10
    },
    font: {
        fontSize: 24
    }
});
export { Header };