import React from 'react';
import {
    Text, View,
    TouchableWithoutFeedback
} from 'react-native';
import {
    CardSection
} from './common';
import { connect } from 'react-redux';

class ListItem extends React.Component {
    constructor(props){
        super(props)
    }
    render() {
        const { id, title } = this.props.library;
        return (
            <TouchableWithoutFeedback
                onPress={() => this.props.selectedLibraryId(id)}
            >
                <View>
                    <CardSection>
                        <Text style={{ marginHorizontal: 15, marginTop: 15, borderBottomWidth: 1 }}>
                            {title}
                        </Text>
                    </CardSection>
                </View>
            </TouchableWithoutFeedback>
        );
    };
};

export default connect(null)(ListItem);