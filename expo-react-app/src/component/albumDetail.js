import React from 'react';
import {
    View, Text, Image, Linking
} from 'react-native';
import {
    Card, CardSection, Button
} from './common';
import {
    IMAGE
} from '../../assets/images';

const AlbumDetail = ({ album }) => {
    const { title, artist, image, url } = album;
    return (
        <Card>
            <CardSection>
                <View>
                <Image style={{ width: 50, height: 50 }}source={IMAGE.image_al}/>
                </View>
                <View>
                <Text>{title}</Text>
                <Text>{artist}</Text>
                </View>
            </CardSection>
            <CardSection>
                <Image style={{ width: null, height: 300, flex: 1 }} source={{ uri: image }}/>
            </CardSection>
            <CardSection>
                <Button name='Buy' onPress={() => Linking.openURL(url)}/>
            </CardSection>
        </Card>
    );
};
export default AlbumDetail;