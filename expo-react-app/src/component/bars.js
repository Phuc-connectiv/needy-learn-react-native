import React from 'react';
import {
    View, Text, ScrollView, ImageBackground, Image
} from 'react-native';
import { IMAGE } from '../../assets/images';
import { DrawerNavigatorItems } from 'react-navigation-drawer';

const Bars = (props) => {
    return (
        <ScrollView>
            <ImageBackground
                source={IMAGE.image_al}
                style={{ width: undefined, padding: 16, paddingTop: 48 }}
            >
                <Image 
                    source={ IMAGE.image_al }
                    style={ styles.barsStyle }
                />
                <View>
                <Text style={styles.nameStyle}>Em Gái Thái</Text>
                </View>
            </ImageBackground>
            <View>
                <DrawerNavigatorItems {...props} />
            </View>
        </ScrollView>
    );
};
export default Bars;
const styles = {
    container: {
        flex: 1
    },
    barsStyle: {
        width: 80,
        height: 80,
        borderRadius: 40,
        borderWidth: 3,
        borderColor: '#fff'
    },
    nameStyle: {
        fontSize: 24,
        color: "#fff",
        fontWeight: 'bold'
    }
};