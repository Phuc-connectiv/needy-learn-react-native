import React, { useState, useContext } from 'react';
import { 
    View, Text 
} from 'react-native';
import { Context } from '../context/blogContext';
import BlogPostForm from '../component/blogPostForm';

const EditScreen = ({ navigation }) => {
    const id = navigation.getParam('id');
    const { state, editBlogPost } = useContext(Context);
    const blogPost = state.find((blogPost) => blogPost.id === id);
    return (
        <BlogPostForm 
            initialValue={{ id: blogPost.id, title: blogPost.title, context: blogPost.context }}
            onSubmit = {( title, context ) => {
                editBlogPost( id, title, context, () => {
                    navigation.navigate('Show');
                })
            }}
        />
    );
};
export default EditScreen;