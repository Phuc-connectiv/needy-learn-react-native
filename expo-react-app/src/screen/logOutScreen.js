import React from 'react';
import {
    View, Text, StyleSheet
} from 'react-native';
import LoginForm from '../component/loginForm';
import firebase from 'firebase';
import {
    Spinner, Button,
    Card, CardSection
} from '../component/common';

class LogoutScreen extends React.Component {
    constructor(props) {
        super(props)
    };
    state = { loggedIn: null }
    componentDidMount() {
        const firebaseConfig = {
            apiKey: 'AIzaSyDxpZEjnv-1QAkN8yspL_rk1en2Nv0OAzQ',
            authDomain: 'manager-7cc3f.firebaseapp.com',
            databaseURL: 'https://manager-7cc3f.firebaseio.com',
            projectId: 'manager-7cc3f',
            storageBucket: 'manager-7cc3f.appspot.com',
            messagingSenderId: '111742288550',
            appId: '1:111742288550:web:b1e4370c2cb7010bb9ea67',
            measurementId: 'G-Y160L6YVKE'
        };
        if (!firebase.apps.length) {
            firebase.initializeApp(firebaseConfig);
        }
        firebase.auth().onAuthStateChanged((user) => {
            if (user) {
                this.setState({ loggedIn: true });
            } else {
                this.setState({ loggedIn: false });
            }
        });

    };
    renderContext() {
        console.log('loggedIn ===> ', this.state.loggedIn);
        switch (this.state.loggedIn) {
            case true:
                return (
                    <View style={styles.container}>
                        <Card>
                            <CardSection>
                        <Button name="Log Out"
                            onPress={() => firebase.auth().signOut()}
                        />
                        </CardSection>
                        </Card>
                    </View>
                );
            case false:
                return <LoginForm />;
            default:
                return <Spinner size='large' />;
        }
    };
    render() {
        return (
            <View>
                {this.renderContext()}
            </View>
        );
    };
};
const styles = StyleSheet.create({
    container: {
        marginVertical: 100
    },
    textInputStyle: {
        width: 100,
        height: 40
    },
    errorStyle: {
        fontSize: 20,
        color: 'red',
        alignSelf: 'center'
    }
});
export default LogoutScreen;