import React from 'react';
import {
    Image, ImageBackground,
    View, ScrollView
} from 'react-native';
import { IMAGE } from '../../assets/images';
import {
    Card, CardSection, Button
} from '../component/common'

const ImageScreen = ({ navigation }) => {
    return (
        <ScrollView>
            <Card>
                <CardSection>
                    <ImageBackground
                        source={IMAGE.image_al}
                        style={{ width: undefined, height: 300, padding: 20, flex: 1 }}
                    >
                        <Image
                            source={IMAGE.image_al}
                            style={{ width: 100, height: 100 }}
                        />
                    </ImageBackground>
                </CardSection>
                <CardSection>
                    <Button 
                    name="LOVE" 
                    onPress={() => navigation.navigate('Index')}
                    />
                </CardSection>
            </Card>
        </ScrollView>
    );
};
export default ImageScreen;