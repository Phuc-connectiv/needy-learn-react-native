// import React, { useState } from 'react';
// import {
//     ScrollView,
//     Text, View,
//     Dimensions,
//     StyleSheet,
//     ImageBackground,
//     TouchableOpacity
// } from 'react-native';
// import { IMAGE } from '../../assets/images';


// let screenWidth = Dimensions.get('window').width;
// let screenHeight = Dimensions.get('window').height;
// let scrollX = 0;
// const IntroScreen = ({ navigation }) => {
//     scrollTo()
//     return (
//         <ImageBackground
//             source={IMAGE.image_girl}
//             style={{ width: screenWidth, height: screenHeight, flex: 1 }}
//         >
//             <ScrollView
//                 ref={(scroller) => { scroller = scroller }}
//                 horizontal={true}
//                 pagingEnabled={true}
//                 showsHorizontalScrollIndicator={true}
//                 scrollIndicatorInsets={{ top: 10, left: 10, bottom: 10, right: 10 }} // *using ios
//                 onMomentumScrollBegin={() => {
//                     console.log('begin slide')
//                 }}
//                 onMomentumScrollEnd={() => {
//                     console.log('End sile');
//                 }}
//                 onScroll={(event) => {
//                     let logData = `Scrolled to x = ${event.nativeEvent.contentOffset.x}, y = ${event.nativeEvent.contentOffset.y}`
//                     console.log(logData);
//                 }}
//                 scrollEventThrottle={10} // *using ios
//             >
//                 <View
//                     style={styles.slideStyle_1}
//                 >
//                     <ImageBackground
//                         source={IMAGE.image_girl}
//                         style={{ width: screenWidth, height: screenHeight, padding: 20, flex: 1 }}
//                     >
//                         <View style={styles.buttonStyle}>
//                             <TouchableOpacity
//                                 onPress={() => {
//                                     scrollX = this.state.screenHeight * 1;
//                                     this.scroller.scrollTo({ x: 0, y: scrollX });
//                                 }}
//                             >
//                                 <View style={styles.nextStyle}>
//                                     <Text style={styles.textViewStyle}>
//                                         Next
//                                     </Text>
//                                 </View>
//                             </TouchableOpacity>
//                         </View>
//                     </ImageBackground>
//                 </View>
//                 <View
//                     style={styles.slideStyle_2}
//                 >
//                     <ImageBackground
//                         source={IMAGE.image_al}
//                         style={{ width: screenWidth, height: screenHeight, padding: 20, flex: 1 }}
//                     >
//                         <View style={styles.buttonStyle}>
//                             <TouchableOpacity>
//                                 <View style={styles.nextStyle}>
//                                     <Text style={styles.textViewStyle}>
//                                         Next
//                                     </Text>
//                                 </View>
//                             </TouchableOpacity>
//                         </View>
//                     </ImageBackground>
//                 </View>
//                 <View
//                     style={styles.slideStyle_3}
//                 >
//                     <ImageBackground
//                         source={IMAGE.image_2}
//                         style={{ width: screenWidth, height: screenHeight, padding: 20, flex: 1 }}
//                     >
//                         <View style={styles.buttonStyle}>
//                             <TouchableOpacity
//                                 onPress={() => navigation.navigate('After')}
//                             >
//                                 <View style={styles.nextStyle}>
//                                     <Text style={styles.textViewStyle}>
//                                         START
//                                     </Text>
//                                 </View>
//                             </TouchableOpacity>
//                         </View>
//                     </ImageBackground>
//                 </View>
//             </ScrollView>
//         </ImageBackground>
//     );
// };
// const styles = StyleSheet.create({
//     slideStyle_1: {
//         backgroundColor: '#5f9ea0',
//         flex: 1,
//         marginTop: 20,
//         width: screenWidth,
//         justifyContent: 'center',
//         alignItems: 'center'
//     },
//     slideStyle_2: {
//         backgroundColor: 'tomato',
//         flex: 1,
//         marginTop: 20,
//         width: screenWidth,
//         justifyContent: 'center',
//         alignItems: 'center'
//     },
//     slideStyle_3: {
//         backgroundColor: '#663399',
//         flex: 1,
//         marginTop: 20,
//         width: screenWidth,
//         justifyContent: 'center',
//         alignItems: 'center'
//     },
//     textStyle: {
//         fontSize: 20,
//         padding: 15,
//         color: 'white',
//         textAlign: 'center'
//     },
//     buttonStyle: {
//         justifyContent: 'flex-end',
//         flex: 1,
//     },
//     nextStyle: {
//         borderRadius: 10,
//         backgroundColor: '#ffffff',
//         opacity: 0.5,
//         height: 40,
//         justifyContent: 'center',
//         alignItems: 'center'
//     },
//     textViewStyle: {
//         fontSize: 24,
//         opacity: 1,
//         fontWeight: 'bold',
//         color: 'black'
//     }
// });
// export default IntroScreen;



import React, { Component } from 'react';
import {
    Dimensions,
    ScrollView,
    StyleSheet,
    Text,
    TouchableOpacity,
    View,
    ImageBackground
} from 'react-native';
import { IMAGE } from '../../assets/images'

let scrollX = 0;

export default class IntroScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            screenHeight: Dimensions.get('window').height,
            screenWidth: Dimensions.get('window').width,
        };
    }

    scrollToSlide1 = () => {
        scrollX = this.state.screenWidth * 1;
        this.scroller.scrollTo({ x: scrollX, y: 0 });
    };
    scrollToSlide2 = () => {
        scrollX = this.state.screenWidth * 2;
        this.scroller.scrollTo({ x: scrollX, y: 0 });
    };
    // scrollToTop = () => {
    //     this.scroller.scrollTo({ x: 0, y: 0 });
    // };

    render() {
        return (
            <ImageBackground
                source={IMAGE.image_girl}
                style={{ width: this.props.screenWidth, height: this.props.screenHeight, flex: 1 }}
            >
                <ScrollView
                    style={styles.container}
                    ref={(scroller) => { this.scroller = scroller }}
                    horizontal={true}
                    pagingEnabled={true}
                    showsHorizontalScrollIndicator={false}
                    scrollIndicatorInsets={{ top: 10, left: 10, bottom: 10, right: 10 }} // *using ios
                    onMomentumScrollBegin={() => {
                        console.log('begin slide')
                    }}
                    onMomentumScrollEnd={() => {
                        console.log('End sile');
                    }}
                    onScroll={(event) => {
                        let logData = `Scrolled to x = ${event.nativeEvent.contentOffset.x}, y = ${event.nativeEvent.contentOffset.y}`
                        console.log(logData);
                    }}
                    scrollEventThrottle={10} // *using ios
                >
                    <View
                        style={styles.slideStyle_1}
                    >
                        <ImageBackground
                            source={IMAGE.image_girl}
                            style={{ width: this.state.screenWidth, height: this.state.screenHeight, padding: 20, flex: 1 }}
                        >
                            <View style={styles.buttonStyle}>
                                <TouchableOpacity
                                    onPress={this.scrollToSlide1}
                                >
                                    <View style={styles.nextStyle}>
                                        <Text style={styles.textViewStyle}>
                                            Next
                                    </Text>
                                    </View>
                                </TouchableOpacity>
                            </View>
                        </ImageBackground>
                    </View>
                    <View
                        style={styles.slideStyle_2}
                    >
                        <ImageBackground
                            source={IMAGE.image_al}
                            style={{ width: this.state.screenWidth, height: this.state.screenHeight, padding: 20, flex: 1 }}
                        >
                            <View style={styles.buttonStyle}>
                                <TouchableOpacity
                                    onPress={this.scrollToSlide2}
                                >
                                    <View style={styles.nextStyle}>
                                        <Text style={styles.textViewStyle}>
                                            Next
                                    </Text>
                                    </View>
                                </TouchableOpacity>
                            </View>
                        </ImageBackground>
                    </View>
                    <View
                        style={styles.slideStyle_3}
                    >
                        <ImageBackground
                            source={IMAGE.image_2}
                            style={{ width: this.state.screenWidth, height: this.state.screenHeight, padding: 20, flex: 1 }}
                        >
                            <View style={styles.buttonStyle}>
                                <TouchableOpacity
                                    onPress={() => this.props.navigation.navigate('Login')}
                                >
                                    <View style={styles.nextStyle}>
                                        <Text style={styles.textViewStyle}>
                                            START
                                    </Text>
                                    </View>
                                </TouchableOpacity>
                            </View>
                        </ImageBackground>
                    </View>
                </ScrollView>
            </ImageBackground>
        );
    }
}

const styles = StyleSheet.create({
    slideStyle_1: {
        backgroundColor: '#5f9ea0',
        flex: 1,
        marginTop: 20,
        width: Dimensions.get('window').width,
        justifyContent: 'center',
        alignItems: 'center'
    },
    slideStyle_2: {
        backgroundColor: 'tomato',
        flex: 1,
        marginTop: 20,
        width: Dimensions.get('window').width,
        justifyContent: 'center',
        alignItems: 'center'
    },
    slideStyle_3: {
        backgroundColor: '#663399',
        flex: 1,
        marginTop: 20,
        width: Dimensions.get('window').width,
        justifyContent: 'center',
        alignItems: 'center'
    },
    textStyle: {
        fontSize: 20,
        padding: 15,
        color: 'white',
        textAlign: 'center'
    },
    buttonStyle: {
        justifyContent: 'flex-end',
        flex: 1,
    },
    nextStyle: {
        borderRadius: 10,
        backgroundColor: '#ffffff',
        opacity: 0.5,
        height: 40,
        justifyContent: 'center',
        alignItems: 'center'
    },
    textViewStyle: {
        fontSize: 24,
        opacity: 1,
        fontWeight: 'bold',
        color: 'black',
    }
});