import React from 'react';
import {
    View, Text
} from 'react-native';
import {
    Header
} from '../component/common';
import AlbumList  from '../component/albumList';

const AlbumScreen = ({ navigation }) => {
    return (
        <View>
            <Header textHeader="Album list" navigation={navigation}/>
            <AlbumList />
        </View>
    );
};
export default AlbumScreen;