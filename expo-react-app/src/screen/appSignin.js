import React from 'react';
import {
    View, Text
} from 'react-native';
import firebase from 'firebase';
import LoginForm from '../component/loginForm';

class AppSignIn extends React.Component {
    componentDidMount() {
        var config =  {
            apiKey: 'AIzaSyDxpZEjnv-1QAkN8yspL_rk1en2Nv0OAzQ',
            authDomain: 'manager-7cc3f.firebaseapp.com',
            databaseURL: 'https://manager-7cc3f.firebaseio.com',
            projectId: 'manager-7cc3f',
            storageBucket: 'manager-7cc3f.appspot.com',
            messagingSenderId: '111742288550',
            appId: '1:111742288550:web:484ea853fe06428bb9ea67',
            measurementId: 'G-ZFXS1YQXBQ'
          };
          if (!firebase.apps.length) {
            firebase.initializeApp(config);
        }
    };
    render() {
        return (
        
                    <LoginForm />
        );
    };
};
export default AppSignIn;