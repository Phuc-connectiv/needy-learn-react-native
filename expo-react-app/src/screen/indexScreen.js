import React, { useContext, useEffect } from 'react';
import {
    View, Text, StyleSheet, StatusBar,
    FlatList, Button, TouchableOpacity
} from 'react-native';
import { Context } from '../context/blogContext';
import { Feather } from '@expo/vector-icons';

const IndexScreen = ({ navigation }) => {
        const { state, deleteBlogPost, getBlogPost } = useContext(Context);
        useEffect(() => {
            getBlogPost();
            const listener = navigation.addListener('didFocus', () => {
                getBlogPost(); 
            });
            return () => {
                listener.remove();
            }
        }, []);
    return (
        <View style={styles.indexStyle}>
            <StatusBar barStyle="light-content"/>
            <Button title="Add Post"
                onPress={ () => navigation.navigate('Create') }
            />
            <FlatList 
                data={state}
                renderItem={({item}) => {
                    return (
                        <TouchableOpacity
                            onPress={() => navigation.navigate('Show', { id: item.id })}
                        >
                            <View style={styles.row}>
                            <Text style={styles.titleStyle}>{ item.title } - { item.id }</Text>
                            <TouchableOpacity
                                onPress={() => deleteBlogPost(item.id)}
                            >
                                <Feather style={styles.iconStyle} name="trash"/>
                            </TouchableOpacity>
                        </View>
                        </TouchableOpacity>
                    ); 
                }}
                keyExtractor={blogPost => blogPost.title}
            />
        </View>
    );
};

const styles = StyleSheet.create({
    indexStyle: {
        flex: 1,
    },
    row: {
        flex:1,
        alignItems:'stretch',
        flexDirection: 'row',
        justifyContent: 'space-around',
        paddingVertical: 10,
        borderBottomWidth: 1,
        borderColor: 'red'
    },
    titleStyle: {
        fontSize: 24
    },
    iconStyle: {
        fontSize: 24
    }
});
export default IndexScreen;