import React from 'react';
import {
    View, Text
} from 'react-native';
import { Provider } from 'react-redux';
import { createStore } from 'redux';
import LibraryList from '../component/libraryList';

const LibraryScreen = () => {
    return (
    
            <View style={{ flex: 1, marginHorizontal: 15, marginVertical: 15 }}>
                <LibraryList />
            </View>
       
    );
};
export default LibraryScreen;