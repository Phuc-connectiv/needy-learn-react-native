import React from 'react';
import {
    View, Text,
    TouchableOpacity,
    ScrollView
} from 'react-native';
import LoginForm from '../component/loginForm';
import firebase from 'firebase';
import { Spinner } from '../component/common';
import {
    CheckBox, Picker
} from 'native-base';

class LoginScreen extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            loggedIn: null,
            male: false,
            feMale: false,
            option: '',
           
            selected: ''
        }
    };

    componentDidMount() {
        const firebaseConfig = {
            apiKey: 'AIzaSyDxpZEjnv-1QAkN8yspL_rk1en2Nv0OAzQ',
            authDomain: 'manager-7cc3f.firebaseapp.com',
            databaseURL: 'https://manager-7cc3f.firebaseio.com',
            projectId: 'manager-7cc3f',
            storageBucket: 'manager-7cc3f.appspot.com',
            messagingSenderId: '111742288550',
            appId: '1:111742288550:web:b1e4370c2cb7010bb9ea67',
            measurementId: 'G-Y160L6YVKE'
        };
        if (!firebase.apps.length) {
            firebase.initializeApp(firebaseConfig);
        }
        firebase.auth().onAuthStateChanged((user) => {
            if (user) {
                this.setState({ loggedIn: true });
            } else {
                this.setState({ loggedIn: false });
            }
        });

    };
    linkRouter() {
        return this.props.navigation.navigate('Album');
    }
    renderContext() {
        console.log('loggedIn ===> ', this.state.loggedIn);
        switch (this.state.loggedIn) {
            case true:
                return this.linkRouter()
            case false:
                return <LoginForm />;
            default:
                return <Spinner size='large' />;
        }
    };
    onMalePress(option) {
        this.setState({
            male: true,
            feMale: false,
            option
        })
    };
    onFeMalePress(option) {
        this.setState({
            male: false,
            feMale: true,
            option
        })
    };
    renderPicker() {
        let arr = [];
        let data = [
            'Ga','Cho','Ca','Chim'
        ];
        for( let item of data ){
            arr.push(<Picker.Item key={item} label={item} value={item}/>)
        }
        return arr;
    };
    render() {
        return (
            <ScrollView>
                <View style={{ margin: 10 }}>
                    {this.renderContext()}
                    <View style={{ marginTop: 30, flexDirection: 'row' }}>
                        <CheckBox
                            checked={this.state.male}
                            onPress={() => this.onMalePress('male')}
                            color='red'
                        />
                        <Text style={{ marginLeft: 30 }}>Male</Text>
                    </View>
                    <View style={{ marginTop: 30, flexDirection: 'row' }}>
                        <CheckBox
                            checked={this.state.feMale}
                            onPress={() => this.onFeMalePress('female')}
                            color='green'
                        />
                        <Text style={{ marginLeft: 30 }} >FeMale</Text>
                    </View>
                    <Text> Your are {this.state.option}</Text>
                    <View style={{
                        borderWidth: 1,
                        width: "100%",
                        marginTop: 20,
                        borderColor: '#ddd',
                        borderRadius: 5
                    }}>
                        <Picker
                        placeholder="select on item"
                        selectedValue={ this.state.selected }
                        onValueChange={ (value) => this.setState({ selected: value }) }
                        >
                            { this.renderPicker() }
                        </Picker>
                    </View>
                </View>
            </ScrollView>
        );
    };
};
export default LoginScreen;