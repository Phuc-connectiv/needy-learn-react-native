import React, { useContext, useState } from 'react';
import { Context } from '../context/blogContext';
import BlogPostForm from '../component/blogPostForm';

const CreateScreen = ({ navigation }) => {
    const { addBlogPost } = useContext(Context);
    return (
        <BlogPostForm 
            onSubmit={(title, context) => {
                    addBlogPost(title, context, () => {
                        navigation.navigate('Index');
                    })
            }}
        />
    );
}; 
export default CreateScreen
