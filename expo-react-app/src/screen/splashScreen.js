import React from 'react';
import {
    View, Text, StyleSheet,
    Button
} from 'react-native';

class SplashScreen extends React.Component {
    render() {
        return (
            <View style={styles.splashStyle}>
                <Button title="Home" />
                <Text>splash</Text>
            </View>
        );
    };
};
export default SplashScreen;
const styles = StyleSheet.create({
    splashStyle: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    }
});
