// import React, { useState } from 'react';
// import { Alert, StyleSheet, Text, TouchableOpacity, View, Button, Image, CameraRoll } from 'react-native';
// import {
//     ActionSheet, Root
// } from 'native-base';
// import * as ImagePicker from 'expo-image-picker';

// const GetImageScreen = () => {
//     let [selectedImage, setSelectedImage] = useState(null);
//     let takePhotoGallery = async () => {
//         let permissionResult = await ImagePicker.requestCameraRollPermissionsAsync();
//         if (permissionResult.granted === false) {
//             alert('Permission to access camera roll is required!');
//             return;
//         }
//         let pickerResult = await ImagePicker.launchImageLibraryAsync();
//         if (pickerResult.cancelled === true) {
//             return;
//         }
//         setSelectedImage({ localUri: pickerResult.uri });
//     };
//     if (selectedImage !== null) {
//         return (
//             <View style={styles.container}>
//                 <Image
//                     source={{ uri: selectedImage.localUri }}
//                     style={styles.thumbnail}
//                 />
//                 <Button
//                     title="Go back"
//                 />
//             </View>
//         );
//     }
//     const onActionSheet = () => {
//         const OPTION = ['Take Photo Camera', 'choose Photo Gallery', 'Cancel'];
//         ActionSheet.show({
//             options: OPTION, cancelButtonIndex: 2, title: 'select a item'
//         },
//             buttonIndex => {
//                 switch (buttonIndex) {
//                     case 0:

//                         break;
//                     case 1:
//                         takePhotoGallery();
//                         break;
//                     default:
//                         Alert.alert('default');
//                         break;
//                 }
//             }
//         )
//     };
//     const takePhotoCamera = () => {
//         CameraRoll.getPhotos({
//             first: 20,
//             assetType: 'Photos',
//           })
//           .then(r => {
//             this.setState({ photos: r.edges });
//           })
//           .catch((err) => {
//              //Error Loading Images
//           });
//     };
//     return (
//         <Root>
//             <View style={styles.container}>
//                 <Button
//                     title="add Image"
//                     onPress={onActionSheet}
//                 />
//             </View>
//         </Root>
//     );
// };
// const styles = StyleSheet.create({
//     container: {
//         flex: 1,
//         backgroundColor: '#fff',
//         alignItems: 'center',
//         justifyContent: 'center',
//     },
//     thumbnail: {
//         width: 300,
//         height: 300,
//         resizeMode: "contain"
//     },
//     buttonText: {
//         backgroundColor: 'red'
//     }
// });
// export default GetImageScreen;


import React from 'react';
import {
    Button, ImageBackground, View,
    StyleSheet, Dimensions
} from 'react-native';
import * as ImagePicker from 'expo-image-picker';
import * as Permissions from 'expo-permissions';
import {
    ActionSheet, Root
} from 'native-base';

class GetImageScreen extends React.Component {
    state = {
        image: './'
    };
    takeCamera = async () => {
        const permissions = Permissions.CAMERA;
        const { status } = await Permissions.askAsync(permissions);

        console.log('Permission =>', permissions);
        console.log('Status => ', status);
        console.log(`[ pickFromCamera ] ${permissions} access: ${status}`);
        if (status !== 'granted') {
            Sentry.captureException(new Error(`[ pickFromCamera ] ${permissions} access: ${status}`));
        } else {
            const { cancelled, uri } = await ImagePicker.launchCameraAsync().catch(error => console.log({ error }));
            this.setState({ image: uri });
            console.log(image);
        }
    };
    takePicture = async () => {
        await Permissions.askAsync(Permissions.CAMERA);
        const { cancelled, uri } = await ImagePicker.launchImageLibraryAsync({
            allowEditing: false
        });
        this.setState({ image: uri })
    };
    onActionSheet = () => {
        const OPTION = ['Take Photo Camera', 'choose Photo Gallery', 'Cancel'];
        ActionSheet.show({
            options: OPTION, cancelButtonIndex: 2, title: 'select a item'
        },
            buttonIndex => {
                switch (buttonIndex) {
                    case 0:
                        this.takeCamera();
                        break;
                    case 1:
                        this.takePicture();
                        break;
                    default:
                        break;
                }
            }
        )
    };
    render() {
        return (
            <Root>
                <View style={styles.container}>
                    <ImageBackground
                        style={styles.image}
                        source={{ uri: this.state.image }}

                    >
                        <View style={styles.row}>
                            <Button
                                onPress={this.takeCamera}
                                title="Take Camera"
                            />
                            <Button
                                onPress={this.takePicture}
                                title="Take Picture"
                            />
                            <Button
                                onPress={this.onActionSheet}
                                title="add Picture"
                            />
                        </View>
                    </ImageBackground>
                </View>
            </Root>
        );
    };
};
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    image: {
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').height,
    },
    row: {
        flexDirection: 'row',
        flex: 1,
        justifyContent: 'space-evenly',
        alignItems: 'flex-end',
        marginBottom: 50
    }
});
export default GetImageScreen;
